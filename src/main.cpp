#include "HX711.h"
#include "LoadCells.h"
#include "SD.h"
#include <Arduino.h>

LoadCells State;
LoadCells PrevState;

HX711 lc0;
HX711 lc1;
HX711 lc2;
HX711 lc3;

void write2CSV(struct LoadCells &);
void closeFile();
void initFile();

void read_lc0();
void read_lc1();
void read_lc2();
void read_lc3();
double limit(double current, double previous);

// Load Cell Calibration Constants
const double k0 = -518.7;
const double k1 = -633.39;
const double k2 = -202.56;
const double k3 = -295.53;

const int lc_clock = 23;
const int pin_lc0 = 14; // Yellow
const int pin_lc1 = 15; // Green
const int pin_lc2 = 19; // Black
const int pin_lc3 = 20; // White

const int chipSelect = BUILTIN_SDCARD;
File dataFile;

void setup()
{
  delay(1000);
  Serial.print("Initializing SD card...");

  if (!SD.begin(chipSelect))
  {

    Serial.println("initialization failed. Things to check:");

    Serial.println("1. is a card inserted?");

    Serial.println("2. is your wiring correct?");

    Serial.println(
        "3. did you change the chipSelect pin to match your shield or module?");

    Serial.println("Note: press reset or reopen this serial monitor after "
                   "fixing your issue!");

    while (true)
      ;
  }
  Serial.println("initialization done.");
  // Configure load cell data pins as inputs
  pinMode(lc_clock, INPUT);
  pinMode(pin_lc0, INPUT);
  pinMode(pin_lc1, INPUT);
  pinMode(pin_lc2, INPUT);
  pinMode(pin_lc3, INPUT);
  lc0.begin(pin_lc0, lc_clock);
  lc1.begin(pin_lc1, lc_clock);
  lc2.begin(pin_lc2, lc_clock);
  lc3.begin(pin_lc3, lc_clock);

  lc0.set_scale();
  lc1.set_scale();
  lc2.set_scale();
  lc3.set_scale();

  lc0.tare(50);
  Serial.println(".");
  lc1.tare(50);
  Serial.println(".");
  lc2.tare(50);
  Serial.println(".");
  lc3.tare(50);
  Serial.println("Load Cells Tared");

  // Attach ISRs to load cell data pins to read data when available
  attachInterrupt(digitalPinToInterrupt(pin_lc0), read_lc0, LOW);
  attachInterrupt(digitalPinToInterrupt(pin_lc1), read_lc1, LOW);
  attachInterrupt(digitalPinToInterrupt(pin_lc2), read_lc2, LOW);
  attachInterrupt(digitalPinToInterrupt(pin_lc3), read_lc3, LOW);

  Serial.println("Load Cells Initialized");
  //Serial.println("Calibrating");

  // Initializes State variables
  State.lc0 = lc0.get_value();
  State.lc1 = lc1.get_value();
  State.lc2 = lc2.get_value();
  State.lc3 = lc3.get_value();

  String dataString = "lc0,lc1,lc2,lc3";
  File dataFile = SD.open("datalog.csv", FILE_WRITE);

  if (dataFile)
  {
    dataFile.println(dataString);
    dataFile.close();
  }

  else
  {
    Serial.println("error opening datalog.csv");
  }

  delay(10000);
}

void loop()
{
  String dataString = String(State.lc0) + "," + String(State.lc1) + "," +
                      String(State.lc2) + "," + String(State.lc3);
  Serial.println(dataString);
  Serial.println("~+~+~+~+~+~+~+~+~+~+~+~");

  File dataFile = SD.open("datalog.csv", FILE_WRITE);

  if (dataFile)
  {
    dataFile.println(dataString);
    dataFile.close();
    Serial.println(dataString);
  }

  else
  {
    Serial.println("error opening datalog.csv");
  }
}

void read_lc0()
{
  State.lc0 = lc0.get_value();
  State.lc0 = limit(State.lc0, PrevState.lc0);
  PrevState.lc0 = State.lc0;
}
void read_lc1()
{
  State.lc1 = lc1.get_value();
  State.lc1 = limit(State.lc1, PrevState.lc1);
  PrevState.lc1 = State.lc1;
}
void read_lc2()
{
  State.lc2 = lc2.get_value();
  State.lc2 = limit(State.lc2, PrevState.lc2);
  PrevState.lc2 = State.lc2;
}
void read_lc3()
{
  State.lc3 = lc3.get_value();
  State.lc3 = limit(State.lc3, PrevState.lc3);
  PrevState.lc3 = State.lc3;
}

double limit(double current, double previous)
{
  double limit = 1000000;

  if (abs(current - previous) > limit)
    return previous;
  else
    return current;
}