# Static Load Test

The static load test verifies the vehicle and test stand can support the dynamic load caused by the motor during operation. This verification ensures the system will not physically yield during operation. 